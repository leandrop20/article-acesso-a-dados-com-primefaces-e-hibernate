package br.com.devmedia.primefacesehibernate.test;

import java.util.List;

import br.com.devmedia.primefacesehibernate.dao.EmpresaDAO;
import br.com.devmedia.primefacesehibernate.model.Empresa;

public class EmpresaTest {

	public static void main(String[] args) throws Exception {
		EmpresaDAO empresaDAO = new EmpresaDAO();
		
		/*Matriz matriz = new Matriz();
		matriz.setCNPJ("70.200.452.565/0001-10");
		matriz.setEmail("matriz@email.com");
		matriz.setLogradouro("Rua Matriz");
		matriz.setNumero(10);
		matriz.setComplemento("conjunto 10");
		matriz.setMunicipio("S�o Paulo");
		matriz.setUf("SP");
		matriz.setTelefone("11-5566-8899");
		matriz.setRazaoSocial("Matriz XY");
		
		empresaDAO.create(matriz);*/
		
		List<Empresa> empresas = empresaDAO.getAll();
		
		for (Empresa empresa : empresas) {
			System.out.println(empresa.getRazaoSocial());
		}
	}
	
}