package br.com.devmedia.primefacesehibernate.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table
public class Matriz extends Empresa {

	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "matriz", cascade = CascadeType.REMOVE)
	private Set<Filial> filiais;

	public Set<Filial> getFiliais() {
		return filiais;
	}

	public void setFiliais(Set<Filial> filiais) {
		this.filiais = filiais;
	}

	@Override
	public String toString() {
		return "Matriz [filiais=" + filiais + "]";
	}

}