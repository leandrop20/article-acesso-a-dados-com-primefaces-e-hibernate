package br.com.devmedia.primefacesehibernate.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;

import org.apache.log4j.Logger;

import br.com.devmedia.primefacesehibernate.dao.EmpresaDAO;
import br.com.devmedia.primefacesehibernate.model.Empresa;
import br.com.devmedia.primefacesehibernate.model.Matriz;

@ManagedBean
public class EmpresaManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(EmpresaManagedBean.class);

	private String selectedCNPJ;

	private Empresa empresa;
	private List<Empresa> empresas;
	private List<Matriz> matrizes;

	public String getMsg() {
		return "testMsg";
	}
	
	public String getTest() {
		return "testando...";
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public List<Empresa> getEmpresas() {
		return empresas;
	}

	public void setEmpresas(List<Empresa> empresas) {
		this.empresas = empresas;
	}

	public List<Matriz> getMatrizes() {
		return matrizes;
	}

	public void setMatrizes(List<Matriz> matrizes) {
		this.matrizes = matrizes;
	}
	
	public String getSelectedCNPJ() {
		return selectedCNPJ;
	}
	
	private void setMsg(String message) {
		//TODO Auto-generated method stub
	}
	
	public void limpar() {
		log.info("Limpando empresa");
		
		if (this.empresa != null) {
			this.empresa.setRazaoSocial("");
			this.empresa.setCNPJ("");
			this.empresa.setComplemento("");
			this.empresa.setLogradouro("");
			this.empresa.setMunicipio("");
			this.empresa.setUf("");
			this.empresa.setNumero(null);
		}
	}
	
	public List<EmpresaManagedBean> getListaMatrizes() {
		log.info("Listando matrizes");
		
		List<EmpresaManagedBean> empresas = new ArrayList<EmpresaManagedBean>();
		
		try {
			EmpresaDAO empresaDAO = new EmpresaDAO();
			
			for (Matriz matriz : empresaDAO.getMatrizes()) {
				EmpresaManagedBean bean = new EmpresaManagedBean();
				bean.setEmpresa(matriz);
				
				empresas.add(bean);
			}
		} catch (Exception e) {
			this.setMsg(e.getMessage());
			log.error(e);
		}
		
		return empresas;
	}
	
	public List<EmpresaManagedBean> getListaEmpresas() {
		limpar();
		
		log.info("Listando empresa");
		
		List<EmpresaManagedBean> empresas = new ArrayList<EmpresaManagedBean>();
		
		try {
			EmpresaDAO empresaDAO = new EmpresaDAO();
			
			for (Empresa empresa : empresaDAO.getAll()) {
				EmpresaManagedBean bean = new EmpresaManagedBean();
				bean.setEmpresa(empresa);
				
				empresas.add(bean);
			}
		} catch (Exception e) {
			this.setMsg(e.getMessage());
			log.error(e);
		}
		
		return empresas;
	}
	
	public String create() {
		String str = "index";
		
		try {
			EmpresaDAO empresaDAO = new EmpresaDAO();
			empresaDAO.create(this.empresa);
			
			limpar();
			this.setMsg("Empresa cadastrada!");
		} catch (Exception e) {
			this.setMsg(e.getMessage());
			str = "insert";
			log.error(e);
		}
		
		return str;
	}
	
	public String edit() {
		log.info("Editando empresa " + this.getSelectedCNPJ());
		
		EmpresaDAO empresaDAO = new EmpresaDAO();
		Empresa empresa = empresaDAO.getByCNPJ(this.getSelectedCNPJ());
		
		if (empresa != null) {
			this.empresa.setCNPJ(empresa.getCNPJ());
			this.empresa.setRazaoSocial(empresa.getRazaoSocial());
			this.empresa.setMunicipio(empresa.getMunicipio());
			this.empresa.setUf(empresa.getUf());
			this.empresa.setLogradouro(empresa.getLogradouro());
			this.empresa.setComplemento(empresa.getComplemento());
			this.empresa.setNumero(empresa.getNumero());
		} else {
			this.setMsg("Empresa n�o encontrada!");
			log.error("Empresa nao encontrada!");
		}
		
		return "update";
	}
	
	public String update() {
		String str = "index";
		
		try {
			EmpresaDAO empresaDAO = new EmpresaDAO();
			empresaDAO.update(this.empresa);
			
			limpar();
			this.setMsg("Atualizando com sucesso!");
		} catch (Exception e) {
			this.setMsg(e.getMessage());
			str = "deleteUpdate";
			log.error(e);
		}
		
		return str;
	}
	
	public String delete() {
		log.info("Excluindo empresa " + this.getSelectedCNPJ());
		
		try {
			EmpresaDAO empresaDAO = new EmpresaDAO();
			empresaDAO.delete(this.getSelectedCNPJ());
			
			limpar();
			this.setMsg("Exclu�do com sucesso!");
		} catch (Exception e) {
			this.setMsg(e.getMessage());
			log.error(e);
		}
		
		return "index";
	}

}