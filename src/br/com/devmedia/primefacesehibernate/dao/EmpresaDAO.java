package br.com.devmedia.primefacesehibernate.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.devmedia.primefacesehibernate.model.Empresa;
import br.com.devmedia.primefacesehibernate.model.Filial;
import br.com.devmedia.primefacesehibernate.model.Matriz;
import br.com.devmedia.primefacesehibernate.util.HibernateUtil;

public class EmpresaDAO {

	@SuppressWarnings("unchecked")
	public List<Empresa> getAll() {
		List<Empresa> empresas = new ArrayList<Empresa>();
		Session session = HibernateUtil.getSession();
		
		try {
			empresas = session.createQuery("from Empresa").list();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
		
		return empresas;
	}
	
	@SuppressWarnings("unchecked")
	public List<Matriz> getMatrizes() {
		List<Matriz> matrizes = new ArrayList<Matriz>();
		Session session = HibernateUtil.getSession();
		
		try {
			matrizes = session.createQuery("from Matriz").list();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
		
		return matrizes;
	}
	
	public Empresa getByCNPJ(String cnpj) {
		Empresa empresa = null;
		Session session = HibernateUtil.getSession();
		
		try {
			empresa = (Empresa) session.get(Empresa.class, cnpj);
		} finally {
			session.flush();
			session.close();
		}
		
		return empresa;
	}
	
	public void create(Empresa empresa) throws Exception {
		Transaction tx = null;
		Session session = HibernateUtil.getSession();
		
		try {
			tx = session.getTransaction();
			tx.begin();
			session.save(empresa);
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null) { tx.rollback(); }
			e.printStackTrace();
			throw new Exception("Error ao criar empresa");
		} finally {
			session.flush();
			session.close();
		}
	}
	
	public void update(Empresa empresa) throws Exception {
		Transaction tx = null;
		Session session = HibernateUtil.getSession();
		
		try {
			tx = session.getTransaction();
			tx.begin();
			session.update(empresa);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) { tx.rollback(); }
			e.printStackTrace();
			throw new Exception("Error ao atualizar empresa");
		} finally {
			session.flush();
			session.close();
		}
	}
	
	public void delete(String empresaId) throws Exception {
		Transaction tx = null;
		Session session = HibernateUtil.getSession();
		
		try {
			tx = session.beginTransaction();
			Empresa empresa = (Empresa) session.load(Empresa.class, empresaId);
			
			if (empresa instanceof Filial) {
				((Filial) empresa).setMatriz(null);
			}
			
			session.delete(empresa);
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null) { tx.rollback(); }
			e.printStackTrace();
			throw new Exception("Error ao excluir empresa");
		} finally {
			session.flush();
			session.close();
		}
	}
	
}